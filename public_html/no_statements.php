<?PHP

require_once ( 'php/common.php' ) ;


print get_common_header ( '' , "Items without statements" ) ;
die ( "DEACTIVATED" ) ;
$db = openDB ( 'wikidata' , 'wikidata' ) ;
$lang = get_db_safe ( trim ( get_request ( 'language' , '' ) ) ) ;

$qs = array() ;
$sql = "select epp_entity_id,(select count(*) from pagelinks where pl_from=epp_page_id and pl_namespace=120) AS cnt FROM wb_entity_per_page WHERE epp_entity_type='item'" ;

$item2sites = array() ;
$fh = fopen ( '/data/project/wikidata-todo/top_items_no_statement.tab' , 'r' ) ;
while ( count($item2sites) < 5000 and !feof($fh) ) {
	$s = explode ( "\t" , trim ( fgets ( $fh ) ) ) ;
	if ( count($s) < 2 ) continue ;
	if ( $s[0] == 'ips_item_id' ) continue ;
	$item2sites[$s[0]] = $s[1] ;
}
fclose ( $fh ) ;
if ( count ( $item2sites ) > 0 ) {
	$sql = "SELECT DISTINCT epp_entity_id FROM wb_entity_per_page WHERE epp_entity_id IN (" . implode(',',array_keys($item2sites)) . ") AND NOT EXISTS (select * from pagelinks where pl_from=epp_page_id and pl_namespace=120)" ;
//	$sql .= " AND epp_entity_id IN (" . implode(',',array_keys($item2sites)) . ") HAVING cnt=0" ;
} else {
	$sql .= " having cnt=0 limit 500" ;
}

//print "<pre>TESTING: $sql</pre>" ; myflush(); exit ( 0 ) ;

if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$q = $o->epp_entity_id ;
	$qs[] = $q ;
}
shuffle($qs);
$qs = array_slice ( $qs , 0 , 500 ) ;

$labels = array() ;
if ( $lang == '' ) $langs = "'en','de','es','it','fr'" ;
else $langs = "'$lang'" ;
$sql = "select * from wb_terms where term_entity_id in (" . implode ( ',' , $qs ) . ") and term_type='label' and term_entity_type='item' and term_language in ($langs)" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$q = $o->term_entity_id ;
	if ( isset ( $labels[$q] ) ) {
		if ( $o->term_language == 'en' ) $labels[$q] = $o->term_text ;
	} else $labels[$q] = $o->term_text ;
}

$cnt = 0 ;
print "<ul>" ;
foreach ( $qs AS $q ) {
	$l = $labels[$q] ;
	if ( !isset($l) ) {
		if ( $lang != '' ) continue ;
		$l = 'Q'.$q ;
	}
	print "<li><a href='//www.wikidata.org/wiki/Q$q' target='_blank'>$l</a> " ;
	print "<a href='/reasonator/?q=$q' target='_blank' title='Reasonator'><img src='//upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Reasonator_logo_proposal.png/16px-Reasonator_logo_proposal.png' border=0 /></a> " ;
	print " (Q$q)" ;
	if ( isset($item2sites[$q]) ) print "; " . $item2sites[$q] . " sitelinks" ;
	print "</li>" ;
	$cnt++ ;
	if ( $cnt >= 50 ) break ;
}
print "</ul>" ;

print "<hr/><form method='get' action='?' class='form inline-form'>
Show only items with labels in <input type='text' name='language' value='$lang' />
<input type='submit' class='btn btn-primary' value='doit' />
</form>" ;


print get_common_footer() ;

?>
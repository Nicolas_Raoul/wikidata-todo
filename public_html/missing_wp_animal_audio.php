<?PHP

require_once ( 'php/common.php' ) ;

$sparql = "SELECT ?animal ?file ?article {
  ?animal wdt:P31 wd:Q16521 .
  ?animal wdt:P51 ?file .
  ?article	schema:about ?animal ; schema:isPartOf <https://en.wikipedia.org/>
  }" ;
$j = getSPARQL ( $sparql ) ;

$data = array () ;
foreach ( $j->results->bindings AS $row ) {
	if ( $row->animal->type != 'uri' ) continue ;
	if ( $row->file->type != 'uri' ) continue ;
	if ( $row->article->type != 'uri' ) continue ;
	$page = preg_replace ( '/^.+\/wiki\//' , '' , urldecode($row->article->value) ) ;
	$data[$page] = (object) array (
		'page' => $page ,
		'file' => preg_replace ( '/^.+\/Special:FilePath\//' , '' , urldecode($row->file->value) ) ,
		'q' => preg_replace ( '/^.+\/entity\//' , '' , urldecode($row->animal->value) )
	) ;
}

$pages = array() ;
$db = openDB ( 'en' , 'wikipedia' ) ;
foreach ( $data AS $page => $d ) $pages[$page] = $db->real_escape_string ( $page ) ;

if ( count($pages) > 0 ) {
	$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_title IN ('" . implode("','",$pages) . "')" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM imagelinks WHERE il_from=page_id AND (il_to LIKE '%.flac' OR il_to LIKE '%.oga' OR il_to LIKE '%.ogg'))" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$page = $o->page_title ;
		$data[$page]->missing = true ;
	}
}

print "<h1>Animals with sound on Wikidata, but no sound on enwp</h1>" ;

ksort ( $data ) ;

$cnt = 0 ;
print "<table>" ;
foreach ( $data AS $page => $d ) {
	if ( !$d->missing ) continue ;
	$cnt++ ;
	print "<tr>" ;
	print "<td style='font-family:Courier;text-align:right'>$cnt</td>" ;
	print "<td><a href='https://en.wikipedia.org/wiki/" . myurlencode($page) . "' target='_blank'>" . str_replace('_',' ',$page) . "</a></td>" ;
	print "<td><pre>" . str_replace ( '_' , ' ' , $d->file ) . "</pre></td>" ;
	print "</tr>" ;
}
print "</table>" ;

/*
print "<pre>" ;
print_r ( $data ) ;
print "</pre>" ;
*/

?>
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "php/common.php") ;
require_once ( "php/pagepile.php" ) ;

$callback = get_request ( 'callback' , '' ) ;
$items = trim ( get_request ( 'items' , '' ) );
if ( $items == '' ) $items = array() ;
else $items = explode ( ',' , str_replace("\n",",",$items) ) ;
$format = get_request ( 'format' , 'json' ) ;
$pagepile = get_request ( 'pagepile' , '' ) ;
$lang = get_request ( 'lang' , '' ) ;
$onlylang = get_request ( 'onlylang' , '' ) ;
$langs = array ( 'en' , 'de' , 'fr' , 'es' , 'it' , 'pl' , 'pt' , 'ja' , 'ru' , 'hu' ) ;
$formats = array ( 'html' => "HTML" , 'json' => 'JSON' , 'tsv' => 'Tab-delimited text' , 'wiki' => 'Wikipedia list' ) ;

//header ( 'Content-type: test/plain'); print_r ( $items ) ;
if ( $format == 'html' or ( ($pagepile == '' || !isset($_REQUEST['doit'])) && count($items) == 0 && $callback == '' && $lang.$onlylang == '' ) ) {
	print get_common_header ( '' , 'Get item names' ) ;
	print "<div class='lead'>
	<p>Generate lists of Wikidata item names</p>
	<form method='post' action='?' class='form form-inline inline-form'>
	<p>Language:
	<input name='lang' value='$lang' placeholder='Preferred language code(s)' />
	<i>or</i>
	<input name='onlylang' value='$onlylang' placeholder='Exclusive language code' />
	<br/><small>Preferred language will fall back on " . implode(',',$langs) . "; first available is used</small>
	</p>" ;
	if ( $pagepile_enabeled ) {
		print "<p><input type='text' name='pagepile' value='$pagepile' placeholder='PagePile ID' />, <i>or</i></p>" ;
	}
	print "<p><textarea name='items' style='width:100%' rows=5 placeholder='Wikidata items (one per line)'>" . implode("\n",$items) . "</textarea></p>
	<p>Format:" ;
	foreach ( $formats AS $k => $v ) {
		print " <label><input name='format' value='$k' type='radio'" ;
		if ( $k == $format ) print " checked" ;
		print "> $v</label>" ;
	}
	print " <input type='submit' value='Do it' name='doit' class='btn btn-primary' />
	</p>
	</form>
	</div>" ;
	if ( $format != 'html' ) {
		print get_common_footer() ;
		exit ( 0 ) ;
	}
}

if ( $format == 'json' ) header('Content-type: application/json; charset=utf-8');
if ( $format == 'tsv' or $format == 'wiki' ) header('Content-type: text/plain; charset=UTF-8');

if ( count($items) == 0 and trim($pagepile) != '' ) {
	$pp = new PagePile ( $pagepile ) ;
	$pp = $pp->getAsWikidata() ;
	$pp->each ( function ( $o , $pp ) use ( &$items ) {
		if ( $o->ns != 0 ) return ;
		$items[] = $o->page ;
	} ) ;
}

$iq = array() ;
foreach ( $items AS $i ) {
	$iq[] = preg_replace ( '/\D/' , '' , $i ) ;
}

$o = array ( "status" => "OK" , "i2n" => array() ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;
//$db->set_charset ( 'utf8' ) ;
make_db_safe ( $lang ) ;
make_db_safe ( $onlylang ) ;
if ( $lang != '' ) array_unshift ( $langs , $lang ) ;
if ( $onlylang != '' ) $langs = array ( $onlylang ) ;
$sql = "select DISTINCT term_entity_id,term_language,term_text from wb_terms where term_entity_type='item' and term_type='label' AND term_entity_id IN (" . implode(',',$iq) . ") AND term_language IN ('" . implode ( "','" , $langs ) . "')" ;
//$o['sql'] = $sql ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($r = $result->fetch_object()){
	$s = htmlentities ( trim ( $r->term_text ) ) ;
	if ( $s == '' ) continue ;
	$o['i2n']['Q'.$r->term_entity_id][$r->term_language] = $s ;
}

foreach ( $o['i2n'] AS $q => $v ) {
	foreach ( $langs AS $l ) {
		if ( !isset ( $v[$l] ) ) continue ;
		$o['i2n'][$q] = $v[$l] ;
		break ;
	}
}

foreach ( $iq AS $i ) {
	$q = 'Q'.$i ;
	if ( isset ( $o['i2n'][$q] ) ) continue ;
	$o['i2n'][$q] = $q ; // Fallback if no label
}

if ( $format == 'json' ) {
	if ( $callback != '' ) print $callback.'(';
	print json_encode ( $o ) ;
	if ( $callback != '' ) print ')';
} else if ( $format == 'html' ) {
	print "<table class='table table-condensed table-striped'>" ;
	print "<thead><tr><th>Item</th><th>Label</th></tr></thead><tbody>" ;
	foreach ( $o['i2n'] AS $q => $l ) {
		print "<tr><td><a href='//www.wikidata.org/wiki/$q' target='_blank'>$q</a></td><td>$l</td></tr>\n" ;
	}
	print "</tbody></table>" ;
	print get_common_footer() ;
} else if ( $format == 'tsv' ) {
	foreach ( $o['i2n'] AS $q => $l ) {
		print "$q\t$l\n" ;
	}
} else if ( $format == 'wiki' ) {
	foreach ( $o['i2n'] AS $q => $l ) {
		print "# [[:d:$q|]] [[$l]]\n" ;
	}
}

?>
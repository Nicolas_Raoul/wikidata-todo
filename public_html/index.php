<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( "php/common.php") ;

function annotateItems ( &$pages ) {
	global $lang , $db ;
	$p2 = array() ;
	foreach ( $pages AS $k => $v ) {
		$p2[] = str_replace ( 'Q' , '' , $k ) ;
		$pages[$k] = array() ;
		$pages[$k]['title'] = $k ;
		$pages[$k]['label'] = array() ;
		$pages[$k]['alias'] = array() ;
		$pages[$k]['description'] = array() ;
	}

	$sql = "SELECT term_entity_id,term_text,term_language,term_type FROM wb_terms WHERE term_entity_id IN (" . join(",",$p2) . ") AND term_entity_type='item' AND term_type IN ('label','description','alias')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$q = 'Q' . $o->term_entity_id ;
		$pages[$q][$o->term_type][$o->term_language] = $o->term_text ;
	}
	
	foreach ( $pages AS $k => $v ) {
		$found = false ;
		foreach ( array($lang,'en','de','es','it','fr') AS $l ) {
			if ( !isset ( $v['label'][$l] ) ) continue ;
			$pages[$k]['title'] = $v['label'][$l] ;
			$found = true ;
			break ;
		}
		if ( !$found and count ( $v['label'] ) > 0 ) $pages[$k]['title'] = implode ( "; " , $v['label'] ) ;
	}
	
}

function runQWithNoProp ( $q , $p , $s1 ) {
	global $db , $start , $action , $lang , $max , $bot ;
	$total = 0 ;
	$sql = "select count(*) AS cnt from pagelinks p1 where p1.pl_title='$q' and p1.pl_namespace=0 and p1.pl_from not in (select pl_from from pagelinks p2 where p2.pl_namespace=120 and p2.pl_title IN ('$p','P360'))" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$total = $o->cnt ;
	}
	
	if ( !$bot ) {
		print "<div>There are $total $s1 on Wikidata." ;
	
		if ( $start == '' ) {
			$start = rand ( 0 , $total-$max-1 ) ;
			print " Picking a random batch of $max" ;
		} else {
			print " Starting at pre-defined position $start" ;
		}
		print "...</div>" ;
		myflush();
	} else {
		if ( $start == '' ) $start = rand ( 0 , $total-$max-1 ) ;
	}
	
	$pages = array() ;
	$sql = "select page_title from page,pagelinks p1 where page_namespace=0 and page_id=p1.pl_from and p1.pl_title='$q' and p1.pl_namespace=0 and p1.pl_from not in (select pl_from from pagelinks p2 where p2.pl_namespace=120 and p2.pl_title IN ('$p','P360')) LIMIT $start,$max" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$pages[$o->page_title] = 1 ;
	}
	
	annotateItems ( $pages ) ;
	
	if ( $bot ) {
		$p2 = array() ;
		$p2['total'] = $total ;
		foreach ( $pages AS $k => $v ) {
			$p2['pages'][$k] = $v['title'] ;
		}
		print json_encode ( $p2 ) ;
	} else {
		print "<ul>" ;
		foreach ( $pages AS $k => $v ) {
			print "<li><a target='_blank' href='//wikidata.org/wiki/$k'>" . $v['title'] . "</a></li>" ;
		}
		print "</ul>" ;
		print "<hr/><a href='?action=$action&lang=$lang&start=$start'>Permanlink</a> (kinda...)" ;
	}
}

$action = get_request ( 'action' , '' ) ;
$start = get_request ( 'start' , '' ) ; if ( $start != '' ) $start *= 1 ;
$lang = get_request ( 'lang' , 'en' ) ;
$max = get_request ( 'max' , 100 ) * 1 ;
$bot = get_request ( 'bot' , 0 ) ;
$callback = get_request ( 'callback' , '' ) ;
$country = get_request ( 'country' , '889' ) ;

$db = openDB ( 'wikidata' , '' ) ;

$actions = array (
	'noisin' => array ( 'Q618123' , 'P131' , 'locations with no assigned administrative unit' ) ,
	'nogender' => array ( 'Q5' , 'P21' , 'people with no assigned gender' ) ,
	'noauthor' => array ( 'Q571' , 'P50' , 'books with no assigned author' )
) ;


if ( $bot ) {
	header('Content-type: application/json');
	if ( $callback != '' ) print "$callback(" ;
} else {
	header('Content-type: text/html');
	print get_common_header ( '' , "Wikidata Todo" ) ;
//	myflush() ;
}

if ( $action == '' ) {
	print "<p>There's always something to do on Wikidata, so let's get to work!</p><div><ul>" ;
	foreach ( $actions AS $a => $d ) {
		print "<li><a href='?action=$a'>" . ucfirst ( $d[2] ) . "</a></li>" ;
	}

	print "<li><a href='./no_statements.php'>Items without any statements</a></li>" ;
	print "<li>Taxon but " ;
	print "<a href='/autolist/index.php?language=en&project=wikipedia&category=&depth=12&wdq=claim%5B31%3A16521%5D%20and%20noclaim%5B105%5D&mode=undefined&statementlist=&run=Run&label_contains=&label_contains_not=&chunk_size=10000'>no taxon rank</a>" ;
	print " | <a href='/autolist/index.php?language=en&project=wikipedia&category=&depth=12&wdq=claim%5B31%3A16521%5D%20and%20noclaim%5B171%5D&mode=undefined&statementlist=&run=Run&label_contains=&label_contains_not=&chunk_size=10000'>no parent taxon</a>" ;
	print " | <a href='/autolist/index.php?language=en&project=wikipedia&category=&depth=12&wdq=claim%5B31%3A16521%5D%20and%20noclaim%5B225%5D&mode=undefined&statementlist=&run=Run&label_contains=&label_contains_not=&chunk_size=10000'>no taxon name</a>" ;
	print "</li>" ;

	print "</ul></div>" ;
	
	
	function petscan ( $q ) {
		return 'https://petscan.wmflabs.org/?doit=1&sparql=' . ('SELECT ?item {'.str_replace('"',"%27",$q).'}') ;
	}
	
	$j = (object) array ( 'items' => getSPARQLitems ( 'SELECT ?item { { ?item wdt:P31 wd:Q6256 } UNION { ?item wdt:P31 wd:Q3624078 } UNION { ?item wdt:P31 wd:Q1763527 } }' , 'item' ) ) ;
	$sql = "select term_entity_id,term_text from wb_terms WHERE term_language='en' AND term_type='label' AND term_entity_type='item' AND term_entity_id IN (" . implode(',',$j->items) . ")" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$countries = array() ;
	while($o = $result->fetch_object()){
		$countries[''.$o->term_entity_id] = $o->term_text ;
	}
	asort ( $countries ) ;
	print "<hr/><div class='countries'>" ;
	print "<div>By country: <select id='countries' rows=1>" ;
	foreach ( $countries AS $q => $name ) {
		print "<option value='$q'" . ($q==$country?'selected':'') . ">$name<?option>" ;
	}
	print "</select> (<a href='test' template='./?country=!country!'>Permalink using this country</a>)</div><ul>" ;
//	print '<li><a template="/autolist/?run=1&wdq="></a></li>' ;
	print '<li><a href="test" template="'.petscan('?item wdt:P31 wd:Q5.?item wdt:P27 wd:Q!country! OPTIONAL {?item wdt:P18 ?dummy} FILTER (!bound(?dummy))').'">All citizens without image</a> | ' ;
	print '<a href="test" template="/fist/wdfist/?sparql=SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?item wdt:P27 wd:Q!country! }">but with candidate images</a> <span id="images_people"></span></li>' ;
	
	print '<li><a href="test" template="'.petscan('{{?item wdt:P17 wd:Q!country!} UNION { ?item wdt:P131 wd:Q!country!}} OPTIONAL {?item wdt:P18 ?dummy} FILTER (!bound(?dummy))').'">All places/objects without image</a> | ' ; // /autolist/?run=1&wdq=(CLAIM[17:!country!] OR TREE[!country!][][131]) AND noclaim[18]
	print '<a href="test" template="/fist/wdfist/?sparql=SELECT ?item WHERE {{?item wdt:P17 wd:Q!country!} UNION {?item (wdt:P131)* wd:Q!country!}}">but with candidate images</a> <span id="images_places"></span></li>' ;
	
	print "<li>Citizens without " ;
	print '<a href="test" template="'.petscan('SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?item wdt:P27 wd:Q!country! . OPTIONAL { ?item wdt:P21  ?dummy0 } FILTER(!bound(?dummy0)) }').'">gender</a> | ' ;
	print '<a href="test" template="'.petscan('SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?item wdt:P27 wd:Q!country! . OPTIONAL { ?item wdt:P106 ?dummy0 } FILTER(!bound(?dummy0)) }').'">occupation</a> | ' ;
	print '<a href="test" template="'.petscan('SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?item wdt:P27 wd:Q!country! . OPTIONAL { ?item wdt:P569 ?dummy0 } FILTER(!bound(?dummy0)) }').'">birth date</a> | ' ;
	print '<a href="test" template="'.petscan('SELECT ?item WHERE { ?item wdt:P31 wd:Q5 . ?item wdt:P27 wd:Q!country! . ?item wdt:P569 ?time0 . FILTER ( ?time0 >= "0000-01-01T00:00:00Z"^^xsd:dateTime && ?time0 <= "1900-01-01T00:00:00Z"^^xsd:dateTime ) . OPTIONAL { ?item wdt:P570 ?dummy0 } FILTER(!bound(?dummy0)) }').'" title="(born before 1900)">death date</a></li>' ;

	print '<li><a template="'.petscan('SELECT ?item WHERE { ?item wdt:P17 wd:Q!country! . ?item wdt:P31 ?sub0 . ?sub0 (wdt:P279)* wd:Q82794 . OPTIONAL { ?item wdt:P131 ?dummy1 } FILTER(!bound(?dummy1)) }').'">Place in country, but not in administrative unit</a></li>' ; //  (many false positives)


	print "</ul></div>" ;
?>

<script>
function count_ratio ( query , id ) {
	$('#'+id).html ( '...' ) ;
	var all , with_images ;
	var cnt = 2 ;
	
	function count_ratio_done () {
		cnt-- ;
		if ( cnt > 0 ) return ;
		var h = "<i>" + with_images + " (" + (Math.floor(with_images*1000/all)/10) + "%) of " + all + " items have images</i>" ;
		$('#'+id).html ( h ) ;
	}
	
	function getURL ( query , addendum ) {
		var q = 'SELECT (count(DISTINCT ?item) AS ?cnt) {{' + query + '} ' + addendum + '}' ;
		return 'https://query.wikidata.org/sparql?format=json&query=' + encodeURIComponent(q) ;
	}
	
	$.get ( getURL(query,'') , function ( d1 ) {
		all = d1.results.bindings[0].cnt.value*1 ;
		count_ratio_done() ;
	} ) ;

	$.get ( getURL(query,'. ?item wdt:P18 ?dummy') , function ( d2 ) {
		with_images = d2.results.bindings[0].cnt.value*1 ;
		count_ratio_done() ;
	} ) ;
}

$(document).ready ( function () {
	$('#countries').change ( function () {
		var q = $('#countries').val() ;
		$('div.countries a').each ( function ( k , v ) {
			var a = $(this) ;
			var href = a.attr('template').replace(/!country!/g,q) ;
			a.attr ( { href : href , taget:'_blank' } ) ;
		} ) ;
		
		count_ratio ( '?item wdt:P27 wd:Q!country!'.replace(/!country!/g,q) , 'images_people' ) ;
		count_ratio ( '{?item wdt:P17 wd:Q!country!} UNION { ?item wdt:P131 wd:Q!country!}'.replace(/!country!/g,q) , 'images_places' ) ;
//		count_ratio ( 'CLAIM[27:!country!]'.replace(/!country!/g,q) , 'images_people' ) ;
//		count_ratio ( 'CLAIM[17:!country!] OR TREE[!country!][][131]'.replace(/!country!/g,q) , 'images_places' ) ;
	} ) ;
	$('#countries').change() ;
} ) ;
</script>
	
<?PHP
	
} else if ( isset ( $actions[$action] ) ) {
	runQWithNoProp ( $actions[$action][0] , $actions[$action][1] , $actions[$action][2] ) ;
}

myflush() ;
if ( $bot ) {
	if ( $callback != '' ) print ")" ;
	exit ( 0 ) ;
}
print get_common_footer() ;

?>
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;
include_once ( "php/wikidata.php" ) ;

$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '' ) ) ;
$value = trim ( get_request ( 'value' , '' ) ) ;
$quick = trim ( get_request ( 'quick' , '' ) ) ;

$j = '' ;

if ( $prop != '' and $value != '' ) {
	$sparql = "SELECT ?q { ?q wdt:P$prop \"$value\" }" ;
	$j = (object) array ( 'items' => getSPARQLitems($sparql) ) ;
} else if ( $quick != '' ) {
	$quick = explode ( ':' , $quick , 2 ) ;
	$propname = trim(strtolower(preg_replace('/[ _].*$/','',$quick[0]))) ;
	$value = trim(str_replace('"','',$quick[1])) ;
	$sparql = "SELECT ?property { ?property a wikibase:Property . ?property rdfs:label ?label FILTER (lang(?label) = \"en\") FILTER ( STRSTARTS(lcase(?label),\"$propname\") )}" ;
	$pj = getSPARQLitems ( $sparql , 'property' ) ;
	if ( count($pj) == 1 ) {
		$prop = preg_replace ( '/^.+entity\/P/' , '' , $pj[0] ) ;
		$sparql = "SELECT ?q { ?q wdt:P$prop \"$value\" }" ;
		$j = (object) array ( 'items' => getSPARQLitems($sparql) ) ;
		
	}
#	print $sparql ; print_r ( $pj ) ; exit(0) ;
}

if ( $j != '' ) {
	if ( count ( $j->items ) == 1 ) {
		$server = 'www.wikidata.org' ;
		$page = 'Q' . $j->items[0] ;
		$url = "https://$server/wiki/" . myurlencode($page) ;
		
		$project = strtolower ( trim ( get_request ( 'project' , '' ) ) ) ;
		if ( $project == 'sqid' ) {
			$url = 'https://tools.wmflabs.org/sqid/#/view?id='.$page ;
		} else if ( $project == 'reasonator' ) {
			$url = 'https://tools.wmflabs.org/reasonator/?q='.$page ;
		} else if ( $project != '' ) {
			$wil = new WikidataItemList ;
			$wil->loadItem ( $page ) ;
			$i = $wil->getItem ( $page ) ;
			if ( isset($i) ) {
				$sl = $i->getSitelink ( $project ) ;
				if ( isset($sl) ) {
					$page = $sl ;
					$server = getWebserverForWiki ( $project ) ;
				}
			}
			$url = "https://$server/wiki/" . myurlencode($page) ;
		}
		
		header('Content-type: text/html; charset=UTF-8'); // UTF8 test
		header("Cache-Control: no-cache, must-revalidate");
		print "<html>\n<meta HTTP-EQUIV='REFRESH' content='0; url=$url'></head></html>" ;
		exit ( 0 ) ;
	}
}

print get_common_header ( '' , 'Wikidata Resolver' ) ;

print "<div class='lead'>Redirect to a Wikidata item, based on a string value for a property.</div>" ;

if ( $j != '' and count ( $j->items ) > 1 ) {
	print "<div>Multiple Wikidata items with P$prop:\"$value\" :<ol>" ;
	foreach ( $j->items AS $i ) {
		print "<li><a href='//www.wikidata.org/wiki/Q$i' target='_blank'>Q$i</a></li>" ;
	}
	print "</ol>" ;
}

print "<form method='get' class='form inline-form form-inline' action='?' style='margin-bottom:20px;'>
<input type='text' name='prop' placeholder='Property (e.g. P227)' value='$prop' size='30' />
<input type='text' name='value' placeholder='Value (e.g. 119186187)' value='$value' size='80' />
<input type='submit' value='Find Wikidata item' class='btn btn-primary' />
Use alternative wiki project <input name='project' placeholder='e.g. enwiki' />
</form>" ;

print "<form method='get' class='form inline-form form-inline' action='?'>
Quick lookup <input type='text' id='quick' name='quick' placeholder='e.g. VIAF:12307054' value='$quick' size='120' />
<input type='submit' value='Find Wikidata item' class='btn btn-primary' />
Use alternative wiki project <input name='project' placeholder='e.g. enwiki' />
<br/>(<a href='#' onclick='$(\"#quick\").val(\"VIAF:12307054\");return false'>example</a>)
</form>" ;

print "<p><i>Note:</i> Alternative wiki project may also be <tt>sqid</tt> or <tt>reasonator</tt>.</p>" ;

print get_common_footer() ;

?>
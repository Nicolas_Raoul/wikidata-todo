<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');
set_time_limit ( 60 * 10 ) ; // Seconds
require_once ( 'php/common.php' ) ;

$prop = get_request('prop','');//'106' ;
$dateprop = get_request('dateprop','569') ;
$bucket_size = get_request('bucket_size','50') ;
$root = get_request('root','') ; // Q483501
$exclude = get_request('exclude','') ;

$dateprop = preg_replace ( '/\D/' , '' , $dateprop ) ;
$prop = preg_replace ( '/\D/' , '' , $prop ) ;
$root = preg_replace ( '/\D/' , '' , $root ) ;
$bucket_size *= 1 ;


print get_common_header ( '' , 'Subclass dates' ) ;

print "<div>
<form action='?' method='get'>
<table>
<tr><th>Property</th><td><input type='text' name='prop' value='P$prop' /> (e.g. P106 for '\"occupation\")</td></tr>
<tr><th>Date prop</th><td><input type='text' name='dateprop' value='P$dateprop' /> (P569=birth, P570=death)</td></tr>
<tr><th>Year range</th><td><input type='number' name='bucket_size' value='$bucket_size' /> (bucket size in years)</td></tr>
<tr><th>Root item</th><td><input type='text' name='root' value='Q$root' /> (e.g. Q483501 for artist)</td></tr>
<tr><th>Exclude</th><td><input type='text' name='exclude' value='$exclude' /> (comma-separated list of items, e.g. Q33999 for actor)</td></tr>
<tr><td/><td><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></td></tr>
</table>
</form>
</div>" ;

if ( !isset($_REQUEST['doit']) ) {
	print get_common_footer() ;
	exit();
}

$exclude = explode ( ',' , $exclude ) ;

$url = "q=claim[$prop:(tree[$root][][279])]" ;
foreach ( $exclude AS $e ) {
	$e = preg_replace ( '/\D/' , '' , $e ) ;
	if ( $e == '' ) continue ;
	$url .= " and noclaim[$prop:(tree[$e][][279])]" ;
}
$url .= " and claim[$dateprop]&props=$dateprop" ;

$url = "$wdq_internal_url?" . str_replace(' ','%20',$url) ;

print "<pre>$url</pre>" ;

$j = json_decode ( file_get_contents ( $url ) ) ;


$buckets = array() ;
foreach ( $j->props->$dateprop AS $v ) {
	if ( $v[1] != 'time' ) continue ;
	if ( !preg_match ( '/^\+0000000(\d\d\d\d)-/' , $v[2] , $m ) ) continue ;
	$bucket = floor ( $m[1] / $bucket_size ) * $bucket_size ;
	$buckets[$bucket]++ ;
}

print "<table class='table table-striped'>" ;
print "<thead><tr><th>Bucket (start year; $bucket_size years)</th><th>Count</th></tr></thead>" ;
print "<tbody>" ;
$total = 0 ;
foreach ( $buckets AS $bucket => $count ) {
	print "<tr>" ;
	print "<td>$bucket</td>" ; //  style='text-align:right'
	print "<td style='text-align:right'>$count</td>" ;
	print "</tr>" ;
	$total += $count ;
}
print "</tbody><tfoot><tr><th>Total</th><td style='text-align:right'>$total</td></tr></tfoot></table>" ;

print get_common_footer() ;

?>
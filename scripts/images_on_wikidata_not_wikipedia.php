#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;

$langs = array ( 'de' , 'es' , 'it' , 'ru' , 'nl' , 'ca' , 'en' , 'cy' ) ;
#$langs = array ( 'cy' ) ; # TESTING

$dir = '/data/project/wikidata-todo/public_html/wp_no_image' ;
$head = "<!doctype html>\n<html><head><meta charset='utf-8'></head><body><p><a href='/wikidata-todo/wp_no_image'>All wikis</a></p><p>Last update: " . date('r') . "</p>" ;
$head .= "<p>Pages are listed if they have an image on Wikidata, but no \"page image\", as determined by Wikipedia. If the Wikipedia page already has an image in the header section, you can complain <a target='_blank' href='https://phabricator.wikimedia.org/maniphest/task/create/?projects=MediaWiki-extensions-PageImages'>here</a> (<a target='_blank' href='https://phabricator.wikimedia.org/tag/mediawiki-extensions-pageimages'>open tasks</a>).</p>" ;
$foot = "</body></html>" ;

function cmp ( $a , $b ) {
	return count($b) - count($a) ;
}

$counts = array() ;
foreach ( $langs AS $lang ) {
	$project = 'wikipedia' ;
	$wiki = $lang.'wiki' ;
	$db = openDBwiki ( $wiki ) ;
	
	$data_tmp = array() ;
	
	$counts[$wiki] = 0 ;
#	$sql = 'select distinct page_title from page WHERE NOT EXISTS ( SELECT * FROM imagelinks where il_from=page_id ) and page_namespace=0 and page_is_redirect=0 and page_title in (select distinct(replace(ips_site_page," ","_")) from wikidatawiki_p.wb_entity_per_page,wikidatawiki_p.wb_items_per_site,wikidatawiki_p.pagelinks where ips_site_id="'.$wiki.'" and ips_item_id=epp_entity_id and epp_entity_type="item" and pl_from=epp_page_id and pl_namespace=120 and pl_title="P18")' ;
	$sql = 'select distinct page_title,epp_entity_id from page,wikidatawiki_p.wb_entity_per_page,wikidatawiki_p.wb_items_per_site,wikidatawiki_p.pagelinks WHERE NOT EXISTS ( SELECT * FROM page_props where pp_page=page_id AND pp_propname IN ("page_image","page_image_free") ) and page_namespace=0 and page_is_redirect=0 and page_title=replace(ips_site_page," ","_") and ips_site_id="'.$wiki.'" and ips_item_id=epp_entity_id and epp_entity_type="item" and pl_from=epp_page_id and pl_namespace=120 and pl_title="P18"' ;
#	$sql .= " LIMIT 50" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$io = 'other' ;
		$q = 'Q' . $o->epp_entity_id ;
		if ( isset ( $data_tmp[$q] ) ) continue ;
		$data_tmp[$q] = $o->page_title ;
#		$data[$io][] = $o->page_title ;
//		fwrite ( $fh , "<li><a target='_blank' href='//$lang.$project.org/wiki/".urlencode($o->page_title)."'>".str_replace('_',' ',$o->page_title)."</a></li>" ) ;
		$counts[$wiki]++ ;
	}

	$data = array() ;
	$data_tmp2 = array_keys ( $data_tmp ) ;
	while ( count($data_tmp2) > 0 ) {
		$tmp = array() ;
		while ( count($data_tmp2) > 0 && count($tmp) < 100 ) $tmp[] = array_pop ( $data_tmp2 ) ;
		if ( count($tmp) == 0 ) continue ; // Paranoia
		$sparql = "SELECT ?q ?image ?ioLabel { VALUES ?q { wd:" . implode ( ' wd:' , $tmp ) . " } ?q wdt:P18 ?image OPTIONAL { ?q wdt:P31 ?io } SERVICE wikibase:label { bd:serviceParam wikibase:language \"$lang\" } }" ;
		$j = getSPARQL ( $sparql ) ;
		if ( !isset($j) ) continue ;
		if ( !isset($j->results) ) continue ;
		if ( !isset($j->results->bindings) ) continue ;
		foreach ( $j->results->bindings AS $v ) {
			$io = 'unknown' ;
			if ( isset($v->ioLabel) and $v->ioLabel->type == 'literal' ) $io = $v->ioLabel->value ;
			$q = preg_replace ( '/^.+\//' , '' , $v->q->value ) ;
			$img = preg_replace ( '/^.+\//' , '' , $v->image->value ) ;
//			print "$q\t$img\t$io\n" ;
			if ( !isset($data_tmp[$q]) ) continue ;
			if ( isset($data[$io][$q]) ) continue ;
			$data[$io][$q] = array ( 'q' => $q , 'image' => $img , 'page' => $data_tmp[$q] ) ;
		}
	}
	uasort ( $data , 'cmp' ) ;
	
//	print_r ( $data ) ; exit ( 0 ) ;



	$fn = "$dir/$wiki.html" ;
	$fh = fopen ( "$fn.tmp" , 'w' ) ;
	fwrite ( $fh , $head ) ;
	fwrite ( $fh , "<p>Total: " . $counts[$wiki] . " pages with image candidates.</p>" ) ;

	foreach ( $data AS $io => $pages ) {
		fwrite ( $fh , "<h2>$io</h2><ol>" ) ;
		foreach ( $pages AS $p ) {
			$page = $p['page'] ;
			$q = $p['q'] ;
			$img = $p['image'] ;
			$s = "<li><a target='_blank' href='//$lang.$project.org/wiki/".urlencode($page)."'>".str_replace('_',' ',$page)."</a>" ;
			$s .= " [<a target='_blank' href='https://www.wikidata.org/wiki/$q'>$q</a>]" ;
			$s .= " : " . urldecode($img) . " [<a target='_blank' href='http://commons.wikimedia.org/wiki/File:$img'>Commons</a>]" ;
			$s .= "</li>" ;
			fwrite ( $fh , $s ) ;
		}
		fwrite ( $fh , "</ol>" ) ;
	}

	fwrite ( $fh , $foot ) ;
	fclose ( $fh ) ;
	rename ( "$fn.tmp" , $fn ) ;
}

$fh = fopen ( "$dir/index.html" , 'w' ) ;
fwrite ( $fh , $head ) ;
foreach ( $langs AS $lang ) {
	$wiki = $lang.'wiki' ;
	fwrite ( $fh , "<li><a href='$wiki.html'>$wiki</a> (" . $counts[$wiki] . " pages)</li>" ) ;
}
fwrite ( $fh , $foot ) ;
fclose ( $fh ) ;


?>

#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','1500M');

include_once ( 'public_html/php/common.php' ) ;

$lang = 'en' ;
$proj = 'wikipedia' ;

$site = $lang . ( $proj=='wikipedia'?'wiki':$proj)  ;
$db = openDB ( $lang , $proj ) ;
$sql = "select page_title from page WHERE page_namespace=0" ;

$pages = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');

#$pages = $result->fetch_all () ;

$fp = fopen("$site.tab", 'w');
while($o = $result->fetch_object()){
	fwrite ( $fp , $o->page_title ) ;
}
fclose ( $fp ) ;

?>